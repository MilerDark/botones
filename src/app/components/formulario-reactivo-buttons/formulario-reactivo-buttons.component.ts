import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-formulario-reactivo-buttons',
  templateUrl: './formulario-reactivo-buttons.component.html',
  styleUrls: ['./formulario-reactivo-buttons.component.css']
})
export class FormularioReactivoButtonsComponent implements OnInit {
  form!: FormGroup;
  constructor(private fb: FormBuilder) { 
    this.accionBotones();
  }

get botones(){
  return this.form.get('botones') as FormArray;
}

  ngOnInit(): void {
  }

 
  accionBotones(): void{
    this.form = this.fb.group({
    botones: this.fb.array([
      [],[]
    ])
  });

  }

  agregar():void{
    this.botones.push(this.fb.control(''));
  }

  borrar(i:number):void{
    this.botones.removeAt(i);
  }

  guardar():void{
    if(!this.form.valid){
    return;
    }
    console.log(this.form.value);
    this.limpiar();
    
  }



  limpiar():void{
    this.form.reset({

    })
  }
   
} 


